#include "EventsProc.h"
#include "..\SyncObjWin32\SyncObjWin32.h"

namespace MySyncLib
{
	
UINT EventsProc::StHandlerForBeginThread(void* pParam) {
	EventsProc* pEventsProc = (EventsProc*)pParam;
	if(pEventsProc != NULL) {
		pEventsProc->Handle();
	}
	return 0;
}

EventsProc::EventsProc(size_t thread_cnt) : m_threads(thread_cnt) {	
	m_pSyncObj = new MyWin32CS();
	m_pCv = new MyWin32CV();
	m_bQuit = false;
	for(size_t i = 0; i < m_threads.size(); i++) {
		m_threads[i] = ::AfxBeginThread((AFX_THREADPROC)StHandlerForBeginThread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_threads[i]->m_bAutoDelete = TRUE;
		m_threads[i]->ResumeThread();
	}
}

void EventsProc::Dispatch(Runnable* pRunnable) {
	MySingleLock lock(*m_pSyncObj);
	m_opQueue.push(pRunnable);

	// Manual unlocking is done before notifying, to avoid waking up
    // the waiting thread only to block again (see notify_one for details)
	lock.unlock();

	m_pCv->notify_all();
}

void EventsProc::Handle(void) {
	while(!m_bQuit) {
		Runnable* pRunnable = NULL;
		{
			MySingleLock lock(*m_pSyncObj);
			while(m_opQueue.empty()) {
				m_pCv->wait(lock);
			}
			
			// 這邊可以做存取shared data的事情
			pRunnable = m_opQueue.front();
			m_opQueue.pop();
		}
		
		if(pRunnable != NULL) {
			pRunnable->Run();
			delete pRunnable;
			pRunnable = NULL;
		}
	}
}

EventsProc::~EventsProc() {	
	// Signal to dispatch threads that it's time to wrap up
	m_bQuit = true;
	m_pCv->notify_all();

	// Wait for threads to finish before we exit
	for(size_t i = 0; i < m_threads.size(); i++) {
		::WaitForSingleObject(m_threads[i]->m_hThread, INFINITE);
	}
	
	// Clean jobs
	while(!m_opQueue.empty()) {
		Runnable* pRunnable = m_opQueue.front();
		delete pRunnable;
		pRunnable = NULL;
		m_opQueue.pop();
	}
	
	// clean sync objects
	delete m_pSyncObj;
	m_pSyncObj = NULL;
	
	delete m_pCv;
	m_pCv = NULL;
}

}