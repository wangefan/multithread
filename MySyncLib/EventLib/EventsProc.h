#ifndef MYSYNCLIB_EVENTLIB_EVENTSPROC_h_
#define MYSYNCLIB_EVENTLIB_EVENTSPROC_h_

#include <queue>
#include <afxwin.h>

namespace MySyncLib
{
class MySyncObjBase;
class MyConditionVariableBase;

class Runnable {
public:
// Constructor
	Runnable() {}
	
// Operations	
	virtual void Run() = 0;	
	
// Implementation
	~Runnable() {}
};

class EventsProc {

	static UINT StHandlerForBeginThread(void* pParam);

public:
	
// Constructor
	EventsProc(size_t thread_cnt = 1);

// Operations	
	void Dispatch(Runnable* pRunnable);	
	
// Implementation
	~EventsProc();

// Deleted operations
private:
	EventsProc(const EventsProc& rhs);
	EventsProc& operator=(const EventsProc& rhs);
	EventsProc(EventsProc&& rhs);
	EventsProc& operator=(EventsProc&& rhs);
	
private:
// Private Operations
	void Handle(void);

private:

	MySyncObjBase* 			 m_pSyncObj;
	MyConditionVariableBase* m_pCv;
	std::queue<Runnable*> 	 m_opQueue;
	std::vector<CWinThread*> m_threads;	//Todo: thread不要綁win32
	bool 					 m_bQuit;

};

} // namespace MySyncLib

#endif //MYSYNCLIB_EVENTLIB_EVENTSPROC_h_