#include "SyncObjWin32.h"

namespace MySyncLib
{

/////////////////////////////////////////////////////////////////////////////
// My Win32 critical section object

MyWin32CS::MyWin32CS()
{		
	::InitializeCriticalSection(&m_csNative);
}

bool MyWin32CS::lock()
{
	::EnterCriticalSection(&m_csNative); 
	return true; 
}

bool MyWin32CS::unlock()
{
	::LeaveCriticalSection(&m_csNative); 
	return true; 
}

CRITICAL_SECTION* MyWin32CS::native()
{
	return &m_csNative;
}

MyWin32CS::~MyWin32CS()
{
	::DeleteCriticalSection(&m_csNative);
}	

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Win32 conditional variable

MyWin32CV::MyWin32CV() 
{
	InitializeConditionVariable (&m_CVNative);
}

void MyWin32CV::wait(MySingleLock& lock)
{
	MyWin32CS* pCS = (MyWin32CS*)lock.syncObj();
	if(pCS != NULL) {
		::SleepConditionVariableCS (&m_CVNative, pCS->native(), INFINITE);
	}
}

void MyWin32CV::notify_one()
{
	::WakeConditionVariable (&m_CVNative);
}

void MyWin32CV::notify_all()
{
	::WakeAllConditionVariable(&m_CVNative);
}

/////////////////////////////////////////////////////////////////////////////

} // namespace MySyncLib