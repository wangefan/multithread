#ifndef MYSYNCLIB_SYNCOBJWIN32_SYNCOBJWIN32_h_
#define MYSYNCLIB_SYNCOBJWIN32_SYNCOBJWIN32_h_

#include  "..\SyncObjBase\SyncObjBase.h"
#include <Windows.h>
namespace MySyncLib
{

/////////////////////////////////////////////////////////////////////////////
// My Win32 critical section object

class MyWin32CS : public MySyncObjBase
{
// Members
CRITICAL_SECTION m_csNative;

// Constructor
public:
	explicit MyWin32CS();

// Operations
	virtual bool lock();
	virtual bool unlock();
	CRITICAL_SECTION* native();

// Implementation
public:
	virtual ~MyWin32CS();
};

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Win32 conditional variable
class MyWin32CV : public MyConditionVariableBase
{
// Members
	CONDITION_VARIABLE m_CVNative;

// Constructor
public:
	MyWin32CV();
	
// Operations
public:
	virtual void wait(MySingleLock&);
	virtual void notify_one();
	virtual void notify_all();
	
// Implementation
public:
	virtual ~MyWin32CV() {}
};
/////////////////////////////////////////////////////////////////////////////


}	// namespace MySyncLib

#endif //MYSYNCLIB_SYNCOBJWIN32_SYNCOBJWIN32_h_