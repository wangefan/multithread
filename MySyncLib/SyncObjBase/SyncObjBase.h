#ifndef MYSYNCLIB_SYNCOBJBASE_SYNCOBJBASE_h_
#define MYSYNCLIB_SYNCOBJBASE_SYNCOBJBASE_h_

namespace MySyncLib
{
/////////////////////////////////////////////////////////////////////////////
// MySingleLock

class MySyncObjBase;
class MySingleLock
{
// Constructors
public:
	explicit MySingleLock(MySyncObjBase& syncObj, bool bInitialLock = true);

// Operations
public:
	bool lock();
	bool unlock();
	MySyncObjBase* syncObj();

// Implementation
public:
	~MySingleLock();

protected:
	MySyncObjBase* m_pObject;
	bool    	   m_bAcquired;
};
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Basic synchronization object

class MySyncObjBase
{
	// Constructor
public:
	explicit MySyncObjBase();

	// Operations
	virtual bool lock() = 0;
	virtual bool unlock() = 0;
	template <typename T>	T* native() = 0;

	// Implementation
public:
	virtual ~MySyncObjBase();
};

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Basic conditional variable
class MyConditionVariableBase
{

	// Constructor
public:
	MyConditionVariableBase() {}

	// Operations
public:
	virtual void wait(MySingleLock&) = 0;
	virtual void notify_one() = 0;
	virtual void notify_all() = 0;

	// Implementation
public:
	virtual ~MyConditionVariableBase() {}
};
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Basic Thread
class MyThreadBase
{
	// Constructor
public:
	MyThreadBase() {}

	// Operations
public:

	// Implementation
public:

};
/////////////////////////////////////////////////////////////////////////////

}	// namespace MySyncLib

#endif //MYSYNCLIB_SYNCOBJBASE_SYNCOBJBASE_h_