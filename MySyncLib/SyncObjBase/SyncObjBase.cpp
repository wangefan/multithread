#include "SyncObjBase.h"

namespace MySyncLib
{

/////////////////////////////////////////////////////////////////////////////
// MySingleLock

MySingleLock::MySingleLock(MySyncObjBase& syncObj, bool bInitialLock)
{		
	m_pObject = &syncObj;
	m_bAcquired = false;

	if (bInitialLock) {
		lock();
	}
}

bool MySingleLock::lock()
{
	m_bAcquired = m_pObject->lock();
	return m_bAcquired;
}

bool MySingleLock::unlock()
{
	if (m_bAcquired)
		m_bAcquired = !m_pObject->unlock();

	// successfully unlocking means it isn't acquired
	return !m_bAcquired;
}

MySingleLock::~MySingleLock()
{
	if (m_bAcquired)
		unlock();	
}	

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Basic synchronization object

MySyncObjBase::MySyncObjBase() {

}

MySyncObjBase* MySingleLock::syncObj() {
	return m_pObject;
}

MySyncObjBase::~MySyncObjBase() {

}

/////////////////////////////////////////////////////////////////////////////

} // namespace MySyncLib