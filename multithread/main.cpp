#include <stdio.h>
#include "..\MySyncLib\EventLib\EventsProc.h"
#include "..\MySyncLib\SyncObjWin32\SyncObjWin32.h"

using namespace MySyncLib;

class MyRunnable : public Runnable {
public:
// Constructor
	MyRunnable(char name[200]) {
		strcpy(m_name, name);
	}
	
// Operations	
	virtual void Run() {
		printf("Run MyRunnable(%s), sleep 5secs!\n", m_name);
		::Sleep(2000);
	}
	
// Implementation
	~MyRunnable() {
		
	}
	
// members
private:
	char m_name[200];
};

EventsProc g_evp(10);	
MyWin32CS g_MyWin32CS;

static UINT DispatchThread(LPVOID param);
static UINT ID_COUNT = 0;

int main(void) {
	const int nDispatchThread = 4;
	CWinThread* pThread[nDispatchThread];
	for(int idxThrd = 0; idxThrd < nDispatchThread; ++idxThrd) {
		pThread[idxThrd] = NULL;
		pThread[idxThrd] = ::AfxBeginThread((AFX_THREADPROC)DispatchThread, NULL, THREAD_PRIORITY_BELOW_NORMAL, 0, CREATE_SUSPENDED);
		pThread[idxThrd]->m_bAutoDelete = FALSE;
		pThread[idxThrd]->ResumeThread();
	}
	for(int idxThrd = 0; idxThrd < nDispatchThread; ++idxThrd) {
		::WaitForSingleObject(pThread[idxThrd]->m_hThread, INFINITE);
	}
	return 0;
}

UINT DispatchThread(LPVOID param) {
	int nID = 0;
	{
		MySingleLock myLock(g_MyWin32CS);
		nID = ++ID_COUNT;
	}

	printf("Thread %d begin\n", nID);
	while (true) {
		printf("Thread %d dispatch event begin!\n", nID);
		char r_name[100];
		sprintf(r_name, "%d", nID);
		g_evp.Dispatch(new MyRunnable(r_name));
		printf("Thread %d dispatch event end!\n", nID);
		Sleep(1000);
	}
	printf("Thread %d end\n", nID);
}